package com.sinfloo.demoCrud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrudTareasUsuariosApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrudTareasUsuariosApplication.class, args);
	}

}
