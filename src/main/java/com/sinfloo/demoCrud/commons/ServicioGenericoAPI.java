package com.sinfloo.demoCrud.commons;

import java.io.Serializable;
import java.util.List;

public interface ServicioGenericoAPI<Tipo, ID extends Serializable> {

	Tipo save (Tipo entity); //método de guardar
	
	void delete(ID id);
	
	Tipo get(ID id);
	
	List<Tipo> getAll();
}
