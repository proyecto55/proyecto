package com.sinfloo.demoCrud.commons;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import com.sinfloo.demoCrud.modelo.Usuario;

@Service
public abstract class ServicioGenericoImpl<Tipo, ID extends Serializable> implements ServicioGenericoAPI<Tipo, ID>{

	@Override
	public Tipo save(Tipo entity) {
		// TODO Auto-generated method stub
		return getDao().save(entity);
	}

	@Override
	public void delete(ID id) {
		getDao().deleteById(id);
		
	}

	@Override
	public Tipo get(ID id) {
		Optional<Tipo> obj = getDao().findById(id);
		if (obj.isPresent()) {
			return obj.get();
		}
		return null;
	}

	@Override
	public List<Tipo> getAll() {
		List<Tipo> retornarLista = new ArrayList<>();
		getDao().findAll().forEach(obj -> retornarLista.add(obj));
		return retornarLista;
	}

	public abstract CrudRepository<Tipo, ID> getDao();
}
