package com.sinfloo.demoCrud.controlador;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sinfloo.demoCrud.modelo.Tareas;
import com.sinfloo.demoCrud.modelo.Usuario;
import com.sinfloo.demoCrud.servicio.api.TareasServicioAPI;


@RestController
@RequestMapping(value ="/api/v1/")
@CrossOrigin("*")
public class TareasRestControlador {

	@Autowired
	private TareasServicioAPI tareasServicioAPI;
	
	@GetMapping(value = "/tareas/lista")
	public List<Tareas> getAll(){
		return tareasServicioAPI.getAll();
	}
	
	@PostMapping(value ="/tareas/guardar")
	public ResponseEntity<Tareas> save(@RequestBody Tareas tarea){
		Tareas obj = tareasServicioAPI.save(tarea);
		
		return new ResponseEntity<Tareas>(obj,HttpStatus.OK);
	}
	
	@GetMapping(value ="/tareas/eliminar/{id}")
	public ResponseEntity<Tareas> delete(@PathVariable Long id){
		Tareas usuario = tareasServicioAPI.get(id);
		
		if(usuario != null) {
			tareasServicioAPI.delete(id);
		}else {
			return new ResponseEntity<Tareas>(usuario, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Tareas>(usuario, HttpStatus.OK);
		
	}
}
