package com.sinfloo.demoCrud.controlador;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sinfloo.demoCrud.modelo.Usuario;
import com.sinfloo.demoCrud.servicio.api.UsuarioServicioAPI;

@RestController
@RequestMapping(value ="/api/v1/")
@CrossOrigin("*")
public class UsuarioRestControlador {

	@Autowired
	private UsuarioServicioAPI usuarioServicioAPI;
	
	@GetMapping(value = "/usuarios/lista")
	public List<Usuario> getAll(){
		return usuarioServicioAPI.getAll();
	}
	
	@PostMapping(value ="/usuarios/guardar")
	public ResponseEntity<Usuario> save(@RequestBody Usuario usuario){
		Usuario obj = usuarioServicioAPI.save(usuario);
		
		return new ResponseEntity<Usuario>(obj,HttpStatus.OK);
	}
	
	@GetMapping(value ="/usuarios/eliminar/{id}")
	public ResponseEntity<Usuario> delete(@PathVariable Long id){
		Usuario usuario = usuarioServicioAPI.get(id);
		
		if(usuario != null) {
			usuarioServicioAPI.delete(id);
		}else {
			return new ResponseEntity<Usuario>(usuario, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Usuario>(usuario, HttpStatus.OK);
		
	}
}
