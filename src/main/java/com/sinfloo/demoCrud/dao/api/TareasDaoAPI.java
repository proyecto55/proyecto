package com.sinfloo.demoCrud.dao.api;

import org.springframework.data.repository.CrudRepository;

import com.sinfloo.demoCrud.modelo.Tareas;


public interface TareasDaoAPI extends CrudRepository<Tareas, Long> {

}
