package com.sinfloo.demoCrud.dao.api;

import org.springframework.data.repository.CrudRepository;

import com.sinfloo.demoCrud.modelo.Usuario;

public interface UsuarioDaoAPI extends CrudRepository<Usuario, Long> {

}
