package com.sinfloo.demoCrud.modelo;


import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity //Anotación que permite que la clase sea mapeada contra una tabla de la bd
public class Usuario {

	@Id //Definición de llave primaria
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id_usuario;
	
	@Column
	private String nombre;
	@Column
	private String email;
	@Column
	private String telefono;
	
	@OneToMany(mappedBy = "usuario", cascade = CascadeType.ALL)
	private List<Tareas> tarea;
	
	//Generación de Getter y setter
	public Integer getId_usuario() {
		return id_usuario;
	}
	public void setId_usuario(Integer id_usuario) {
		this.id_usuario = id_usuario;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	
	
	
}

