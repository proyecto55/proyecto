package com.sinfloo.demoCrud.servicio.api;

import com.sinfloo.demoCrud.commons.ServicioGenericoAPI;
import com.sinfloo.demoCrud.modelo.Tareas;

public interface TareasServicioAPI extends  ServicioGenericoAPI<Tareas, Long>{

}
