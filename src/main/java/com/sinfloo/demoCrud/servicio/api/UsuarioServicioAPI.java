package com.sinfloo.demoCrud.servicio.api;

import com.sinfloo.demoCrud.commons.ServicioGenericoAPI;
import com.sinfloo.demoCrud.modelo.Usuario;

//Servicios que nos permitirán modelar la aplicación, se crean clases genéricas para utilizarlo en cualquier servicio de la app
public interface UsuarioServicioAPI extends  ServicioGenericoAPI<Usuario, Long>{

	
}
