package com.sinfloo.demoCrud.servicio.impl;

import com.sinfloo.demoCrud.commons.ServicioGenericoImpl;
import com.sinfloo.demoCrud.modelo.Tareas;

import com.sinfloo.demoCrud.servicio.api.TareasServicioAPI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import com.sinfloo.demoCrud.dao.api.TareasDaoAPI;


@Service
public class TareasServicioImpl extends ServicioGenericoImpl<Tareas, Long> implements TareasServicioAPI {

	@Autowired
	private TareasDaoAPI tareasDaoAPI;
	
	@Override
	public CrudRepository<Tareas, Long> getDao() {
		return tareasDaoAPI;
	}

}
