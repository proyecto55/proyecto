package com.sinfloo.demoCrud.servicio.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import com.sinfloo.demoCrud.commons.ServicioGenericoImpl;
import com.sinfloo.demoCrud.dao.api.UsuarioDaoAPI;
import com.sinfloo.demoCrud.modelo.Usuario;
import com.sinfloo.demoCrud.servicio.api.UsuarioServicioAPI;

@Service
public class UsuarioServicioImpl extends ServicioGenericoImpl<Usuario, Long> implements UsuarioServicioAPI {

	@Autowired
	private UsuarioDaoAPI usuarioDaoAPI;
	
	@Override
	public CrudRepository<Usuario, Long> getDao() {
		return usuarioDaoAPI;
	}

}
